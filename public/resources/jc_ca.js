/*
 * A cellular automation system
 * Writting by Jon Keatley
 * 09/2018
 * 
*/
function JC_CA(_canvas,_width,_height)
{
	"use strict";
		
// public functions ----------------------------------------------------

// Tick actions --------------------------------------------------------
	this.Start = function()
	{
		if(!mIsRunning)
		{
			mIsRunning = true;
			mIntervalRef = setInterval(update,250);
		}
	}
//----------------------------------------------------------------------
	this.Stop = function()
	{
		if(mIntervalRef && mIsRunning === true)
		{
			clearInterval(mIntervalRef);
			mIsRunning = false;
		}
	}
//----------------------------------------------------------------------
	this.Reset = function()
	{
		self.Stop();
		m1DNextLine = 1;
		mGrid = CopyGrid(mSavedGrid);
		mRenderAction();
		
	}
//----------------------------------------------------------------------	
	this.Clear = function()
	{
		self.Stop();
		mGrid = CreateGrid(mWidth,mHeight);
		mSavedGrid = CopyGrid(mGrid);
		mRenderAction();
	}
	
// Other Actions -------------------------------------------------------
	
	this.SetMode = function(_mode)
	{
		switch(_mode.toUpperCase())
		{
			case "WIREWORLD":
				mStates = buildWireWorldState();
				mTickAction = wireworldStep;
				mRenderAction = stateColorRender;
			break;
			case "ANT":
				mStates = buildAntState();
				mTickAction = antStep;
				mRenderAction = stateColorRender;
			break;
			case "1D":
				mStates = build2DState();
				mTickAction = oneDStep;
				mRenderAction = blackWhiteRender;
			break;
			case "CGOL":
			default:
				mStates = buildCGOLState();
				mTickAction = conwaysGOLStep;
				mRenderAction = blackWhiteRender;
			break;
		}
		this.Clear();
		mMode = _mode;
	}
	
//----------------------------------------------------------------------

	this.SetRule = function(_rule)
	{
		m1DRule = _rule;
	}
	
//----------------------------------------------------------------------

	this.GetRule = function()
	{
		return m1DRule;
	}

// cell actions --------------------------------------------------------
	this.SetCell = function(_x,_y,_value)
	{
		if(IsPositionValid(_x,_y))
		{
			mGrid[_y][_x] = _value;
			mRenderAction();
		}
	}
//----------------------------------------------------------------------
	this.ToggleCell = function(_x,_y)
	{
		if(IsPositionValid(_x,_y))
		{
			var count = countStates();
			var nextState = mGrid[_y][_x] + 1;
			if(nextState > count)
			{
				mGrid[_y][_x] = 0;
			}
			else
			{
				mGrid[_y][_x] = nextState;
			}
			
			mRenderAction();
		}
	}
//----------------------------------------------------------------------
	this.GetCell = function(_x,_y)
	{
		if(IsPositionValid(_x,_y))
		{
			return mGrid[_y][_x];
		}
		
		return -1;
	}
	
//----------------------------------------------------------------------

	this.AddExample = function()
	{
		switch(mMode)
		{
			case "WIREWORLD":
				wireworldExample();
			break;
			case "ANT":
				antExample();
			break;
			case "1D":
				oneDExample();
			break;
			case "CGOL":
			default:
				conwayExample();
			break;
		}
		
		mSavedGrid = CopyGrid(mGrid);
		mRenderAction();
	}
	
	
// private functions ---------------------------------------------------
// build examples ------------------------------------------------------

	function antExample()
	{
		mGrid[Math.floor(mGridHeight / 2)][Math.floor(mGridWidth / 2)] = 2;
	}
	
//----------------------------------------------------------------------

	function oneDExample()
	{
		for(var x =0;x < mGridWidth; x++)
		{
			mGrid[0][x] = Math.random() >= 0.51?1:0;				
		} 
	}
	
//----------------------------------------------------------------------

	function conwayExample()
	{
		var pentadecathlon = [];
		pentadecathlon[0 ] = [1,1,1];
		pentadecathlon[1 ] = [0,1,0];
		pentadecathlon[2 ] = [0,1,0];
		pentadecathlon[3 ] = [1,1,1];
		pentadecathlon[4 ] = [0,0,0];
		pentadecathlon[5 ] = [1,1,1];
		pentadecathlon[6 ] = [1,1,1];
		pentadecathlon[7 ] = [0,0,0];
		pentadecathlon[8 ] = [1,1,1];
		pentadecathlon[9 ] = [0,1,0];
		pentadecathlon[10] = [0,1,0];
		pentadecathlon[11] = [1,1,1];
		
		var spaceX = 12;
		var spaceY = 18;
		var offsetX = 4;
		var offsetY = 4;
		var countX = Math.floor(mGridWidth / spaceX);
		var countY = Math.floor(mGridHeight / spaceY);
		
		for(var y=0;y<countY;y++)
        {
            for(var x=0;x<countX;x++)
            {
				if((x * countX) + countX + offsetX < mGridWidth)
				{
					for(var py = 0;py < pentadecathlon.length;py++)
					{
						for(var px=0;px < pentadecathlon[py].length;px++)
						{
							mGrid[(y * spaceY) + py + offsetY][(x * spaceX) + px + offsetX] = pentadecathlon[py][px];
						}
					}
				}
			}
			
			offsetX += y%2 == 0?6:-6;
		}
	}
	
//----------------------------------------------------------------------

	function wireworldExample()
	{
		var xor = [];
		xor[0] = [0,1,1,1, 1,1,1,3, 2,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0];
		xor[1] = [1,0,0,0, 0,0,0,0, 0,1,1,1, 1,1,1,0, 0,0,0,0, 0,0,0,0];
		xor[2] = [0,1,1,1, 2,3,1,1, 1,0,0,0, 0,0,0,1, 0,0,0,0, 0,0,0,0];
		xor[3] = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,1,1, 1,1,0,0, 0,0,0,0];
		xor[4] = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,1,0, 0,1,1,1, 1,1,1,1];
		xor[5] = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,1,1, 1,1,0,0, 0,0,0,0];
		xor[6] = [0,3,2,1, 1,1,1,3, 2,0,0,0, 0,0,0,1, 0,0,0,0, 0,0,0,0];
		xor[7] = [1,0,0,0, 0,0,0,0, 0,1,1,1, 1,1,1,0, 0,0,0,0, 0,0,0,0];
		xor[8] = [0,1,1,1, 1,1,1,1, 1,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0];
		
		var offsetX = Math.floor((mGridWidth / 2) - (xor[0].length / 2));
		var offsetY = Math.floor((mGridHeight / 2) - (xor.length / 2));
		
		for(var y = 0;y < xor.length;y++)
		{
			for(var x=0;x < xor[y].length;x++)
			{
				mGrid[y + offsetY][x + offsetX] = xor[y][x];
			}
		}
	}
	
//----------------------------------------------------------------------
	
// state list methods --------------------------------------------------
	function getState(_state)
	{
		if(mStates.hasOwnProperty("state" + _state))
		{
			return mStates["state" + _state];
		}
		else
		{
			return "";
		}
	}
	
	function hasState(_state)
	{
		return mStates.hasOwnProperty("state" + _state);
	}
	
	function countStates()
	{
		var c =1;
		while(mStates.hasOwnProperty("state" + c))
		{
			c += 1;
		}
		
		return c;
	}
	
	function buildCGOLState()
	{
		return {
			"state1":"#000000"
		};
	}
	
	function buildWireWorldState()
	{
		return {
			"state1":"#00FFFF", //wire
			"state2":"#0000FF", //electron head
			"state3":"#FF0000"  //electron tail
		};
	}
	
	function build2DState()
	{
		return {
			"state1":"#FFFFFF" // live cell
		};
	}
	
	function buildAntState()
	{
		return {
			"state1":"#000000", //on cell
			
			"state2":"#FF3E00", //ant on north
			"state3":"#FF7C00", //ant on east
			"state4":"#FFBA00", //ant on south
			"state5":"#FFF800", //ant on west
			
			"state6":"#003EFF", //ant off north
			"state7":"#007CFF", //ant off east
			"state8":"#00BAFF", //ant off south
			"state9":"#00F8FF", //ant off west
		};
	}
	
// neigbour counting methods -------------------------------------------
	function mooreNeighbourCount(_x,_y,_value)
    {
        var count = 0;
        for(var cy = -1;cy <= 1;cy++)
        {
            for(var cx = -1;cx <= 1;cx++)
            {
                var nx = _x + cx;
                var ny = _y + cy;
               
                if(cy === 0 && cx === 0)
                {
                    continue;
                }
               
                if(nx >= 0 && ny >= 0)
                {
                    if(nx < mGridWidth && ny < mGridHeight)
                    {
                        if(mGrid[ny][nx] === _value)
                        {
                            count += 1;
                        }   
                    }
                }
            }
        }
        
        return count;
    }
    
//----------------------------------------------------------------------

	function wolframCode(_x,_y,_rule)
	{
		var count = 0;
		
		if(_x - 1 > 0) //left
		{
			if(mGrid[_y][_x - 1] >= 1)
			{
				count += 4;
			}
		}
		
		if(mGrid[_y][_x] >= 1) // middle
		{
			count += 2;
		}
		
		if(_x + 1 < mGridWidth) //right
		{
			if(mGrid[_y][_x + 1] >= 1)
			{
				count += 1;
			}
		}
		
		var mask = Math.pow(2,count);
		if((mask & _rule) > 0)
		{
			return 1;
		}
		else
		{
			return 0;
		}		
	}
       
// step rules ----------------------------------------------------------
    function conwaysGOLStep()
    {
        var tmp = [];
        for(var y=0;y<mGridHeight;y++)
        {
            tmp[y] = [];
            for(var x=0;x<mGridWidth;x++)
            {
                var c = mooreNeighbourCount(x,y,1);
               
                if(mGrid[y][x] === 1)
                {
                    if(c < 2)
                    {
                        tmp[y][x] = 0;
                    }
                    else if(c === 2 || c === 3)
                    {
                        tmp[y][x] = 1;
                    }
                    else
                    {
                        tmp[y][x] = 0;
                    }
                }
                else if(c === 3)
                {
                    tmp[y][x] = 1;
                }
                else
                {
                    tmp[y][x] = 0;
                }
            }
        }
       
        mGrid = tmp;
    }
    
//----------------------------------------------------------------------

	function wireworldStep()
    {
        var tmp = [];
        for(var y=0;y<mGridHeight;y++)
        {
            tmp[y] = [];
            for(var x=0;x<mGridWidth;x++)
            {
                var c = mooreNeighbourCount(x,y,2);
                
                if(mGrid[y][x] === 2) //electron head
                {
					tmp[y][x] = 3;
				}
				else if(mGrid[y][x] === 3) //electron tail
				{
					tmp[y][x] = 1;
				}
				else if(mGrid[y][x] === 1) //conductor
				{
					if(c === 1 || c === 2) 
					{
						tmp[y][x] = 2;
					}
					else
					{
						tmp[y][x] = 1;
					}
				}
				else
				{
					tmp[y][x] = 0;
				}
            }
        }
       
        mGrid = tmp;
    }
    
//----------------------------------------------------------------------

	function updateAnt(value,state)
	{
		if(value >= 1)
		{
			return state;
		}
		else
		{
			return state + 4;
		}
	}
	
//----------------------------------------------------------------------

	function getXTargetForDirection(_dir,_x)
	{
		if(_dir === 3 || _dir === 7)
		{
			return _x + 1;
		}
		else if(_dir === 5 || _dir === 9)
		{
			return _x - 1;
		}
		else
		{
			return _x;
		}
	}
	
//----------------------------------------------------------------------

	function getYTargetForDirection(_dir,_y)
	{
		if(_dir === 2 || _dir ===  6)
		{
			return _y - 1;
		}
		else if(_dir === 4 || _dir === 8)
		{
			return _y + 1;
		}
		else
		{
			return _y;
		}
	}
		
//----------------------------------------------------------------------
  
	function getAntDirection(_dir)
	{
		if(_dir >= 6)
		{
			_dir -= 4;
		}
		return _dir;
	}

//----------------------------------------------------------------------
    
    function antStep()
    {
		var tmp = CreateGrid(mGridWidth,mGridHeight);
		
		for(var y=0;y<mGridHeight;y++)
		{
            for(var x=0;x<mGridWidth;x++)
            {
				
				//find ant
				if(mGrid[y][x] >= 2)
				{
					var targetX = 0;
					var targetY = 0;
					var direction = getAntDirection(mGrid[y][x]);
					
					if(mGrid[y][x] >= 6)
					{
						//turn left
						if(direction === 2)
						{
							direction = 5; //turn west
						}
						else
						{
							direction -= 1;
						}
						tmp[y][x] = 1;
						
					}
					else
					{
						//turn right
						if(mGrid[y][x] === 5)
						{
							direction = 2; //turn north
						}
						else
						{
							direction += 1;
						}
						tmp[y][x] = 0;
					}
					
					targetX = getXTargetForDirection(direction,x);
					targetY = getYTargetForDirection(direction,y);
										
					if(IsPositionValid(targetY,targetX))
					{
						tmp[targetY][targetX] = updateAnt(mGrid[targetY][targetX],direction);
					}
					else
					{
						console.log("can not move");
						tmp[y][x] = updateAnt(mGrid[y][x],direction);
					}
				}
				else
				{
					if(tmp[y][x] === 0)
					{
						tmp[y][x] = mGrid[y][x];
					}
				}
            }
        }
        
        mGrid = tmp;
	}
    
//----------------------------------------------------------------------

	function oneDStep()
	{
		var tmp = CopyGrid(mGrid);
		
		var seedLine = m1DNextLine - 1;
		if(seedLine < 0)
		{
			seedLine = mGridHeight - 1;
		}
		
		for(var x=0;x<mGridWidth;x++)
        {
			tmp[m1DNextLine][x] = wolframCode(x,seedLine,m1DRule);
		}
		
		m1DNextLine += 1;
		if(m1DNextLine >= mGridHeight)
		{
			m1DNextLine = 0;
		}
		
		 mGrid = tmp;
	}
    
// render methods ------------------------------------------------------  
    function blackWhiteRender()
    {
        mContext.fillStyle = "#FFFFFF";
        mContext.fillRect(0,0,mWidth,mHeight);
        mContext.fillStyle = "#000000";
       
        for(var y=0;y<mGridHeight;y++)
        {
            for(var x=0;x<mGridWidth;x++)
            {
                if(hasState(mGrid[y][x]))
                {   
                    var dx = x * mCellWidth;
                    var dy = y * mCellHeight;
                    mContext.fillRect(dx + 1,dy + 1, mCellWidth - 1, mCellHeight - 1);
                }
            }
        }
    }
//----------------------------------------------------------------------  
    function stateColorRender()
    {
        mContext.fillStyle = "#FFFFFF";
        mContext.fillRect(0,0,mWidth,mHeight);
       
        for(var y=0;y<mGridHeight;y++)
        {
            for(var x=0;x<mGridWidth;x++)
            {
                if(hasState(mGrid[y][x]))
                {   
                    var dx = x * mCellWidth;
                    var dy = y * mCellHeight;
                  
                    mContext.fillStyle = getState(mGrid[y][x]);
                    mContext.fillRect(dx + 1,dy + 1, mCellWidth - 1, mCellHeight - 1);
                }
            }
        }
    }

//----------------------------------------------------------------------
	function update()
	{
		mTickAction();
		mRenderAction();
	}
    
//----------------------------------------------------------------------  
    var mTickAction = function()
    {
		console.log("tick not set");
	}
//----------------------------------------------------------------------
    var mRenderAction = function()
    {
		console.log("render not set");
	}
//----------------------------------------------------------------------
	var IsPositionValid = function(_x,_y)
	{
		if(_x >= 0 && _x < mGridWidth)
		{
			if(_y >= 0 && _y < mGridHeight)
			{
				return true;
			}
		}
		
		return false;
	}
//----------------------------------------------------------------------
	var CreateGrid = function(_width,_height)
	{
		var grid = new Array(_height);

		for(var i =0; i < _height; i++)
		{
			grid[i] = new Array(_width);
			for(var j=0; j < _width;j++)
			{
				grid[i][j] = 0;
			}
		}
		
		return grid;
	}
//----------------------------------------------------------------------
	var CopyGrid = function(_copy)
	{
		var grid = new Array(mGridHeight);
		
		for(var i=0;i< mGridHeight;i++)
		{
			grid[i] = new Array(mGridWidth);
			for(var j=0;j < mGridWidth;j++)
			{
				grid[i][j] = _copy[i][j];
			}
		}
		
		return grid;
	}
//mouse actions --------------------------------------------------------
	var moveMouse = function(_event)
	{
		if(!mIsRunning)
		{
			var actualWidth = mCanvas.getBoundingClientRect().width;
		
			var pX = Math.floor(_event.offsetX / mCellWidth);
			var pY = Math.floor(_event.offsetY / mCellHeight);
			
			if(mWidth > actualWidth)
			{
				var scaledMouse = _event.offsetX / (actualWidth / mWidth);
				pX = Math.floor(scaledMouse / mCellWidth );
				
				scaledMouse =  _event.offsetY / (mCanvas.getBoundingClientRect().height / mHeight);
				pY = Math.floor(scaledMouse / mCellHeight);
			}
			
			if(IsPositionValid(pX,pY))
			{
				mMouseX = pX;
				mMouseY = pY;
				mRenderAction();
				renderMouse();
			}
		}
	}
//----------------------------------------------------------------------
	var mouseClick = function(_event)
	{
		if(!mIsRunning)
		{
			self.ToggleCell(mMouseX,mMouseY);
			mSavedGrid = CopyGrid(mGrid);
		}
		
		return false; //prevent bubbling
	}
//----------------------------------------------------------------------
	var renderMouse = function()
	{
		if(IsPositionValid(mMouseX,mMouseY))
		{
			var dx = mMouseX * mCellWidth;
			var dy = mMouseY * mCellHeight;
		  
			mContext.strokeStyle = "rgba(5, 56, 107, 1)";
			mContext.strokeRect(dx + 2,dy + 2, mCellWidth - 2, mCellHeight - 2);
		}
	}	
// init ----------------------------------------------------------------
	
	var self = this;
	var mCanvas = _canvas;
	var mContext = mCanvas.getContext("2d");
	var mGridWidth = _width;
	var mGridHeight = _height;
	var mGrid = CreateGrid(mGridWidth,mGridHeight);
	var mSavedGrid = CopyGrid(mGrid);
	var mIntervalRef = null;
	
	var mWidth = parseInt(mCanvas.getAttribute("width"));
	var mHeight = parseInt(mCanvas.getAttribute("height"));
	
	var mCellWidth = mWidth / mGridWidth;
    var mCellHeight = mHeight / mGridHeight;
    
    var mStates = buildCGOLState();
    
    var mMouseX = -1;
    var mMouseY = -1;
    
    var mIsRunning = false;
    
    
    var mMode = "CGOL";
    this.SetMode(mMode);
    
    //1D specific variables
    var m1DRule = 105;
    var m1DNextLine = 1;
	
	mCanvas.onmousemove  = moveMouse;
	mCanvas.onmouseup = mouseClick;
}
