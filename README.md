# CA.Js - V0.2.0
## A Javascript Cellular automation library

Currently supports 4 different types of CA :-
- Conways Game of life
- Wireworld
- Langton's Ant
- Elementary (1D CA)

A live demo of this library can be found here http://jon.keatley.folio.gitlab.io/ca.js/
