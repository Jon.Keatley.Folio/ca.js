import { CACore, CAState, CARender } from "./cacore";

export class BasicRender implements CARender {
    
    render(ca:CACore, canvas:HTMLCanvasElement)
    {
        const deadState = 0;
        const state:readonly CAState[][] = ca.getStateGrid();
        const width: number =  canvas.width;
        const height: number = canvas.height;
        const cellWidth: number = Math.ceil(width / ca.gridWidth);
        const cellHeight: number = Math.ceil(height / ca.gridHeight);
        const render: CanvasRenderingContext2D | null = canvas.getContext("2d");
        if(render === null)
        {
            console.log(`Unable to access 2d context for canvas ${canvas.id}`)
            return;
        }
        
        render.fillStyle = "#FFFFFF";
        render.fillRect(0, 0, width, height);
        for(let y =0; y < ca.gridHeight; y++)
        {
            for( let x = 0; x < ca.gridWidth; x++)
            {
                if(state[y][x].id > deadState)
                {
                    render.fillStyle = state[y][x].color;
                    render.fillRect(x * cellWidth,y * cellHeight,cellWidth,cellHeight);
                }
            }
        }
    }
}