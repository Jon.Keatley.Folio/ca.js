import { CACore } from './cacore'

const STATE_DEAD  = 0;
const STATE_ALIVE = 1;

class ConwaysGOL extends CACore {

    constructor(width: number, height:number) {
        super(width, height);
        
        this.addState(this.emptyState);
        this.addState({id:STATE_ALIVE, name:"alive", color:"#000000"});
    }
    
    update() {
        const tmp = this.createGrid(this.gridWidth, this.gridHeight, STATE_DEAD);
        for(let y=0;y<this.gridHeight;y++)
        {
            for(let x=0;x<this.gridWidth;x++)
            {
                const neighbours = this.mooreNeighbourCount(x,y, 1);
               
                if(this.getCellValue(x, y) === STATE_ALIVE)
                {
                    if(neighbours < 2)
                    {
                        tmp[y][x] = 0;
                    }
                    else if(neighbours === 2 || neighbours === 3)
                    {
                        tmp[y][x] = 1;
                    }
                    else
                    {
                        tmp[y][x] = 0;
                    }
                }
                else if(neighbours === 3)
                {
                    tmp[y][x] = 1;
                }
                else
                {
                    tmp[y][x] = 0;
                }
            }
        }
       
        this.replaceGrid(tmp)
    }
}

export { ConwaysGOL }