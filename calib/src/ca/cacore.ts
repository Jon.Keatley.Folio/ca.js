/*
 * A cellular automation system - core class
 * Writting by Jon Keatley
 * 2023-01
 * 
*/

export interface CARender { 
  render( ca:CACore, canvas:HTMLCanvasElement): void;
}

export interface CAState {
  id: number,
  name: string,
  color: string
}

interface CAStates {
  [key: number]: CAState
}

export const STATE_NOT_FOUND: CAState = { id:-1, name:"not found", color:"#000000"};

class CACore {
  grid: number[][];
  states: CAStates;
  emptyState: CAState = { id:0, name:"empty", color:"#FFFFFF" };
  gridWidth:number;
  gridHeight:number;
  
  constructor(width:number, height:number)
  {
    this.gridWidth = width;
    this.gridHeight = height;
    this.grid = this.createGrid(width, height);
    this.states = {};
  } 
  
  //Grid methods ---------------------
  createGrid(width:number, height:number, fillValue = 0): number[][] {
    const newGrid = new Array(height);
    for(let y=0; y < height; y++)
    {
      newGrid[y] = new Array(width).fill(fillValue);
    }
    
    return newGrid;
  }
  
  copyGrid(gridSource = this.grid): number[][] {
    const clone: number[][] = new Array(0);
    gridSource.map(row => clone.push(Object.assign([], row)));
    return clone;
  }
  
  replaceGrid(newGrid: number[][]) {
    this.grid = newGrid;
  }
  
  getStateGrid(gridSource = this.grid): readonly CAState[][] {
    const stateGrid: CAState[][] = new Array(0);
    
    gridSource.map(row => {
      const stateRow: CAState[] = new Array(0);
      row.map(cell => stateRow.push(Object.assign({}, this.getState(cell))))
      stateGrid.push(stateRow);
    })
    
    return stateGrid;
  }
  
  // Cell methods ----------------------
  isValidCell(x:number, y:number): boolean {
    if(y < 0 || y >= this.gridHeight)
    {
      return false;
    }
    
    if(x < 0 || x >= this.gridWidth)
    {
      return false;
    }
    
    return true;
  }
  
  getCellValue(x:number, y:number): number {
    
    if(!this.isValidCell(x,y))
    {
      return -1;
    }
    
    return this.grid[y][x];
  }
  
  setCellValue(x:number, y:number, val =0): boolean {
    
    if(!this.isValidCell(x,y))
    {
      return false;
    }
    
    this.grid[y][x] = val;
    return true;
  }
  
  //CA methods --------------------------
  mooreNeighbourCount(x:number, y:number, target = 0) {
    //TODO - think about improving this!
    let count = 0;
    for(let cy = -1;cy <= 1;cy++)
    {
      for(let cx = -1;cx <= 1;cx++)
      {
        if (cx === 0 && cy === 0)
        {
         continue;
        }
        
        const val = this.getCellValue(x + cx, y + cy);   
        if(val === target)
        {
          count += 1;
        }   
      }
    }
        
    return count;
  }
  
  //state methods -------------------------
  addState(state: CAState)
  {
    this.states[state.id] = state;
  }
  
  hasState(stateId: number)
  {
    return this.states[stateId] !== undefined;
  }
  
  getState(stateId: number) : CAState
  {
    return this.states[stateId];
  }
  
  getCellState(x:number, y:number): CAState
  {
    if(!this.isValidCell(x, y))
    {
      return STATE_NOT_FOUND;
    }
    
    return this.states[this.getCellValue(x, y)];
  }

  //abstract methods --------------------
  update()
  {
    console.log("update needs to be implemented");
  }
}

export { CACore };


