import React, { FC, useRef, useEffect } from 'react';

import { CACore, CARender } from '../ca/cacore';

import "./ca.css"

/*
hooks needed
- draw CA on start - X
- time out for updating the CA - x
- mouse move 
- mouse click

Not sure how to solve 
- select CA rule -> prop change?
- reset simulation
- save current state -> outside of this component?

*/

export interface CAProps {
  ca: CACore;
  renderer: CARender;
  refresh:number;
}

const CellularAutomation: FC<CAProps> = (props:CAProps) => {
    const canvasRef = useRef<HTMLCanvasElement>(null);
    let timerId: ReturnType<typeof setInterval>;
    
    useEffect(() => {
        //set timer for updating the simulation
        timerId = setInterval(() => {
            props.ca.update();
            if(canvasRef.current === null)
            {
                return;
            }
            props.renderer.render(props.ca, canvasRef.current);
        },props.refresh);
        
        //draw initial state
        if(canvasRef.current !== null)
        {
            props.renderer.render(props.ca, canvasRef.current);
        }
        
        return () => clearInterval(timerId);
        
    })
    
    return (<canvas width="400" height="400" ref={canvasRef} >TODO add no canvas message</canvas>);
}

export default CellularAutomation;