import React from 'react';
import ReactDOM from 'react-dom/client';

import { BasicRender } from './ca/cacanvas';
import { ConwaysGOL } from './ca/gol';
import CellularAutomation from './components/ca';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
const gol: ConwaysGOL = new ConwaysGOL(30,30);
gol.setCellValue(1,1,1);
gol.setCellValue(2,1,1);
gol.setCellValue(3,1,1);

const render:BasicRender = new BasicRender();

const refresh = 2000;
root.render(
  <React.StrictMode>
    <CellularAutomation ca={gol} renderer={render} refresh={refresh} />
  </React.StrictMode>
);

