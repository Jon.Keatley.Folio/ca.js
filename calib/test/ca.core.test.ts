import { expect, test } from 'vitest'

import { STATE_NOT_FOUND, CAState, CACore } from '../src/ca/cacore'

test('CA Core - grid creation', () => {
    let core = new CACore(5,5);
    //valid cell locations
    expect(core.getCellValue(0,0)).toEqual(0);
    expect(core.getCellValue(4,4)).toEqual(0);
    //invalid cell locations
    expect(core.getCellValue(5,0)).toEqual(-1);
    expect(core.getCellValue(0,5)).toEqual(-1);
})

test('CA Core - check valid cells', () => {
    let core = new CACore(5,10);
    
    expect(core.isValidCell(0,0)).toBeTruthy();
    expect(core.isValidCell(1,0)).toBeTruthy();
    expect(core.isValidCell(2,0)).toBeTruthy();
    expect(core.isValidCell(3,0)).toBeTruthy();
    expect(core.isValidCell(4,0)).toBeTruthy();
    
    expect(core.isValidCell(0,9)).toBeTruthy();
    
    expect(core.isValidCell(-1,-1)).toBeFalsy();
    expect(core.isValidCell(5,0)).toBeFalsy();
    expect(core.isValidCell(0,10)).toBeFalsy();
})

test('CA Core - set valid cell value', () => {
    let core = new CACore(5,5);
    expect(core.getCellValue(0,0)).toEqual(0);
    expect(core.setCellValue(0,0,1)).toBeTruthy();
    expect(core.getCellValue(0,0)).toEqual(1);
})

test('CA Core - set invalid cell value', () => {
    let core = new CACore(5,5);
    expect(core.setCellValue(-1,-1,1)).toBeFalsy();
    expect(core.setCellValue(5,0,1)).toBeFalsy();
    expect(core.setCellValue(0,5,1)).toBeFalsy();
})

test('CA Core - grid copy', () => {
    let core = new CACore(5,5);
    let clone = core.copyGrid();
    expect(core.setCellValue(0,0,1)).toBeTruthy();
    expect(core.getCellValue(0,0)).toEqual(1);
    expect(clone[0][0]).toEqual(0);
})

test('CA Core - moores neighbour count', () => {
    let core = new CACore(3,3);

    core.setCellValue(0,0,2);
    core.setCellValue(1,0,2);
    core.setCellValue(2,0,2);
    expect(core.mooreNeighbourCount(1,1,2)).toEqual(3);
    
    core.setCellValue(0,2,2);
    core.setCellValue(1,2,2);
    core.setCellValue(2,2,2);
    
    expect(core.mooreNeighbourCount(1,1,2)).toEqual(6);
     
    core.setCellValue(0,1,2);
    core.setCellValue(2,1,2);
    
    expect(core.mooreNeighbourCount(1,1,2)).toEqual(8);   
})

test('CA Core - add state', () => {
    let core = new CACore(3,3);
    
    core.addState({id:1, name:"test", color:"#FF00FF"});
    expect(core.hasState(1)).toBeTruthy();
    expect(core.hasState(-1)).toBeFalsy();
})

test('CA Core - get cell state', () => {
    let core = new CACore(3,3);
    let state: CAState = {id:2, name:"test", color:"#FF00FF"}
    core.addState(state);
    core.setCellValue(0,0, state.id);
    
    expect(core.getCellState(0, 0)).toBe(state);
    expect(core.getCellState(-1,-1)).toBe(STATE_NOT_FOUND);
    
    // what to do for default state 
})
