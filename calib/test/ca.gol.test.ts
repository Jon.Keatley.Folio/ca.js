import { expect, test } from 'vitest'

import { ConwaysGOL } from '../src/ca/gol'

test('GOL - test step with blinker oscillator',() => {
    //tests the blinker oscillator - tests all rules except overcrowding
    
    const gol:ConwaysGOL = new ConwaysGOL(8,8);
    
    gol.setCellValue(0,1,1);
    gol.setCellValue(1,1,1);
    gol.setCellValue(2,1,1);
    
    gol.update();
    
    expect(gol.getCellValue(1,0)).toEqual(1);
    expect(gol.getCellValue(1,1)).toEqual(1);
    expect(gol.getCellValue(1,2)).toEqual(1);
    
    gol.update();
    
    expect(gol.getCellValue(0,1)).toEqual(1);
    expect(gol.getCellValue(1,1)).toEqual(1);
    expect(gol.getCellValue(2,1)).toEqual(1);
})

test('GOL - test step with beacon oscillator',() => {
    //tests the beacon oscillator for overcrowding logic - tests all rules except lonelyness
    
    const gol:ConwaysGOL = new ConwaysGOL(8,8);
    
    gol.setCellValue(1,1,1);
    gol.setCellValue(2,1,1);
    gol.setCellValue(1,2,1);
    gol.setCellValue(2,2,1);
    
    gol.setCellValue(3,3,1);
    gol.setCellValue(4,3,1);
    gol.setCellValue(3,4,1);
    gol.setCellValue(4,4,1);
    
    gol.update();
    
    expect(gol.getCellValue(2,2)).toEqual(0);
    expect(gol.getCellValue(3,3)).toEqual(0);
    
    gol.update();
    
    expect(gol.getCellValue(2,2)).toEqual(1);
    expect(gol.getCellValue(3,3)).toEqual(1);
})